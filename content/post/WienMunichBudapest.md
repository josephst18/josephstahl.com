+++
categories = ["Vienna"]
date = "2015-06-30T20:05:44-05:00"
draft = false
description = "Beer in Munich, paprika in Budapest, and trains all over Europe. This week I traveled around central Europe and saw the incredible size of the Holy Roman Empire."
keywords = ["article", "Vienna", "Austria", "Munich", "Germany", "Budapest", "Hungary", "Travel", "Europe"]
leadpicture = "https://josephstahl.com/media/WienMunichBudapest/heropicture.jpg"
title = "Munich and Budapest"

+++

![Fountain at Schloss Nymphenburg](/media/WienMunichBudapest/heropicture.jpg)
*The gardens at Schloss Nymphenburg*

Beer in Munich, paprika in Budapest, and trains all over Europe. This week I traveled around central Europe and saw the incredible size of the Holy Roman Empire.

<!--more-->

# Munich
May 15th was a short day of classes, to allow everyone to travel to their travel-weekend cities of choice Friday afternoon. The class met in the morning to visit the Museum of Modern Art (in the Museumsquartier, close to the Museum of Natural History) which ended up being my favorite museum of the trip. Most of the museum was exhibiting a collection called "Ludwig Goes Pop", a massive exhibit of pop art belonging to Peter and Irene Ludwig. The collection includes several of the most famous pieces of pop art, including works by Andy Warhol (Marilyn Monroe, Campbell's Soup) and Roy Lichtenstein.

![Love artwork](/media/WienMunichBudapest/WienMunichBudapest-1.jpg)
*A Lichtenstein work in MUMOK*

After the museum visit I returned to the Hotel Donauwalzer to get my bag before heading to Westbahnhof to take the RailJet to Munich. I almost ended up in Innsbruck because (unknown to me) the train was divided into sections, with the front of the train going to Vienna and the rear half splitting off at a stop to go to Innsbruck. According to my phone's GPS app, the train reached a top speed of >130 mph, easily setting the record for my fastest rail trip in Europe.

## Friday evening in Munich
Once in Germany my phone's data stopped working, leaving me with only a cached map to get to the Euro Youth Hostel. I dropped off my bags there, then walked with John (who I was traveling with) to his hostel and then to dinner after he dropped off his backpack. We originally tried to eat at the Augustiner Biergarten but could not even find two seats despite the bench-style seating on account of it being Friday night at a popular beer garden. We ended up eating at a local restaurant instead then walking downtown to the Neues Rathaus. 

## Saturday
My first night in a hostel went better than I expected, although the experience is nothing like a hotel. I showered the night before but did not pack a towel, so I instead dried off with a spare shirt. Lights were turned off in my 12-person room at midnight and lights came on around 7:45pm as people woke up.

I met John Saturday morning at the Hauptbahnhof to buy public transit tickets and get breakfast. After purchasing a three-day pass, we headed downtown to start sightseeing. The Munich subway system is exceptionally large, with subway tickets divided into three levels, with the smallest (the Innerraum) covering only the area in and immediately around Munich, and the most expensive ticket providing transportation all the way to Dachau and Schloss Neuschwanstein. 


![Neues Rathaus](/media/WienMunichBudapest/WienMunichBudapest-2.jpg)
*The Neues Rathaus. In the clock tower are several statuettes that perform an elaborate "dance" several times a day as the bells chime.*

My sightseeing started at the Neus Rathaus, a massive Gothic building and the historic city hall of Munich. From there I saw Franenkirche, Alter Peter, and the Englischer Garten. The Englischer Garten covers approximately 1.4 square miles in the southeast of Munich. The area is known for several attractions, including the Chinese Tower and a surfer-friendly river with a standing wave. 

![Englischer Garten](/media/WienMunichBudapest/WienMunichBudapest-3.jpg)
*Looking out over the English Garden*

After lunch, we walked to the Deutsches Museum, which is best described as a Smithsonian-esque museum dedicated to science. The exhibits range from full-size replicas of ships in the sailing exhibit, to airplane cockpits, to solar electricity control panels. I particularly enjoyed the history of music exhibit, which included an original alto saxophone designed and built by Adolph Sax! The museum felt massive despite several exhibits being closed for renovation and was a welcome relief from walking around on an increasingly painful ankle. 

That evening we were determined to visit the Augustiner Brewery, so John and I left the Deutsches Museum to go to dinner around 4:30pm and arrived before the crowd. I allowed the waitress to pick my beer; I ended up a 0.5L glass of the excellent Augustiner Helles Bock. Easily my favorite beer I had in Europe!  I had a sausage plate and pretzel with my meal, rounding out about as German a meal as I could have had.

![German food!](/media/WienMunichBudapest/WienMunichBudapest-4.jpg)
*German food! Sausages, pretzel, beer, sauerkraut.*

## Sunday
Sunday started with a trip to the Olympic Park in northeast Munich, partly to see the Olympic facilities leftover from the 1972 Summer Olympics, but (for me) mainly to visit the BMW Museum. For a car lover, the museum is amazing, functioning both as a showroom and museum of all things BMW. The architecture of the building took my breath away; after two weeks of Historicist and Modernist architecture, a glass and steel building with cavernous spaces was unlike anything I'd seen in Europe so far. Even better, the museum, because it was also a showroom, allows visitors to sit in cars (except the BMW i8) and on motorcycles. 

The Olympic Stadium and surroundings are still in frequent use today; the swimming pool was full of lap swimmers when I visited and a marathon was taking place on the surrounding pathways. After leaving the park I traveled to Allianz Arena, the home stadium of FC Bayern and another example of modern (i.e. present-day) architecture. 

![NS Museum](/media/WienMunichBudapest/WienMunichBudapest-5.jpg)
*The Nazi museum in Munich ("NS-Dokumentationszentrum")*

Closer to central Munich, I visited the Nazi Museum, a relatively new museum chronicling the Nazi Party's (NSDAP) rise to power in Germany. The museum offers free admission on Sundays and the crowd there made it apparent that I was not the only one taking advantage of this. 

## Monday

![Gardens of Schloss Nymphenberg](/media/WienMunichBudapest/WienMunichBudapest-6.jpg)
*A fountain at the very back of the garden at Schloss Nymphenberg*

Monday marked my final day in Munich, so I traveled with my suitcase until my afternoon train back to Vienna. Schloss Neuschwanstein (the "Disney" castle) was too far away to visit in half a day, but plenty of castles are more easily reached. I traveled by tram to Schloss Nymphenberg, a beautiful building that is more palace than castle. An artificial river and flower garden lay in front of the palace, with the rest of the artificial river and a huge garden behind the palace. The landscaped part of the garden, with grass and pathways, is very narrow and about a mile long, with the rest of the garden filled by a forest and botanical garden. 

Before we boarded our train back, John and I made one final stop at perhaps Munich's most famous restaurant, the Hofbrauhaus. The place is deservedly famous for its lengthy history but has devolved into a tourist trap, albeit a tourist trap with excellent (but overpriced) food. Overall, I liked the Augustiner more, especially their beer. Yet despite catering to tourists, the Hofbrauhaus refuses to split bills for individuals, an experience that is unpleasantly common in Europe. 

We got back to Vienna around 8:00pm after only a little confusion in Salzburg where we almost boarded the wrong train. Overall, the trip to Munich was delightful and a good contrast with Vienna. Munich, likely because of its bombing in World War II, has many modern buildings alongside its historical ones. 

# Budapest
Because of scheduling constraints, the class trip departed for Budapest (briefly the headquarters of the Holy Roman Empire) the day after everyone got back from their weekend trips. After an unremarkable train ride there, Professor Zeller bought public transit tickets for everyone and we rode the bus to City Hotel Matyas where we were to stay for the next two nights. 

Despite being a member of the European Union, Hungary uses their own currency, the Hungarian forint. The exchange rate is vastly dissimilar to that of the Euro or pound, with (at the time) 1 USD approximately equal to 270 HUF. My first experience at an ATM there, where I withdrew HUF 10,000, was certainly a new experience! 

![St. Stephen's Basilica](/media/WienMunichBudapest/WienMunichBudapest-7.jpg)
*St. Stephen's Basilica*

Our limited time in Budapest meant we were on a very tight schedule and began touring and learning that afternoon. We first visited St. Stephen's Basilica and climbed the stairs (**many** of them) to the top of the dome where we could see the entire city. Budapest reminds me of the Dallas/ Ft. Worth area in that Budapest is technically two cities combined, with Buda (and City Hotel Matyas) on one side of the Danube and Pest on the other. The interior of the church, like most in Europe, was decorated in a highly Baroque style complete with gold gilding, marble, and stained glass throughout. On a historical and slightly gross note, the church houses a relic of its namesake: the hand of St. Stephen. 

After the church the class visited a massive covered marketplace, one of Budapest's landmarks. Inside, stores sell meat, peppers (the "paprika" that is famous in Hungary), fruits and vegetables, souvenirs, baked goods, and just about everything else. 

Brandon and I were both craving Mexican food after several weeks away from Chipotle so we ate dinner at a Hungarian Mexican restaurant which, unsurprisingly, did not live up to the standards I'm used to. Afterwards we returned to the hotel and met up with our class to celebrate our TA's birthday at a well-known cocktail bar. The decorations of the bar caught my attention; it clearly emulates Prohibition-era American speakeasies despite having no obvious connection to the United States. Regardless, the excellent drinks excused any unusual decorations. 

## Wednesday
Wednesday started by walking over the Danube to reach the Pest side of Budapest, where the class took a cable car to the top of a hill that looks over all of Buda. The royal palace (no longer in use today; Budapest has no royal family) stands at the top of the hill and today houses the Budapest History Museum. The museum, while interesting, leans heavily towards portraying Budapest and Hungary in a favorable light. So while the exhibits are interesting and factually correct, there are several parts of Budapest history that are exempt from the museum so that unfavorable history is not taught to visitors. 

![View of Parliament from Fishermen's Bastion](/media/WienMunichBudapest/WienMunichBudapest-8.jpg)
*The Hungarian Parliament across the Danube, as seen from Fisherman's Bastion*

That afternoon the class met at the Museum of Modern Art, a museum which is endowed by the same Peter Ludwig of "Ludwig Goes Pop" fame. 

## Thursday
Thursday morning started out rainy, so I was glad we took a tour of the Hungarian Parliament that morning. Like the Museum of Budapest History, the guided tour of Parliament was designed to highlight all of Hungary's grandest achievements and spent no time at all discussing less proud moments of Hungarian history (such as their complicity in Jewish deportations during the Holocaust). The policing at both the museum and Parliament struck me as unusual; soldiers in uniform (rather than civilian police) patrolled at each location. 

![Hungarian Parliament](/media/WienMunichBudapest/WienMunichBudapest-9.jpg)
*Hungarian Parliament on a rainy day*

Before our trip to the train station we stopped at one more art museum, this one a museum dedicated to Hungarian modern art. Finally, we boarded the train back to Vienna, arriving exhausted in the mid-evening. 

# Closing thoughts
## Munich
- More modern buildings than Vienna, possibly because of extensive bombing in World War II. 
- The Englischer Garten is incredible. Massive park in the heart of the city, much like Central Park in NY only larger and with a more varied landscape. 
- Similar to Vienna, with a historical city center (Neues Rathaus, Frauenkirche) and new suburbs (BMW World, Olympic Park)
- English is widespread there; I never encountered a language barrier.


## Budapest
- Buildings are very historic, although extremely dirty due to pollution during Soviet occupation, ending 1991. 
- Also on account of Soviet occupation, many parts of the city are visibly older/ dirtier/ more run-down than Vienna and Munich. 
- English is far less commonplace, with fewer speakers and the English speakers that are there speaking it less fluently. 
- Lives up to its reputation as the City of Views. Between the dome on St. Stephen's Basilica and the hill where the royal palace resides, getting a view of the entire city is extraordinarily easy. 
- Far cheaper than Munich or Vienna. I spent only 36 USD over three days.