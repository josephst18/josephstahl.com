+++
date = "2015-05-03T10:56:44-05:00"
draft = false
title = "Traveling to Vienna"
categories = [ "Vienna" ]

+++

It's my first day of travel! <!--more--> 

## Auf Wiedersehen, Birmingham!
I'm writing this in the Birmingham Airport, awaiting my flight to Atlanta. I found out this morning that I'm the first in my immediate family to leave North America; neither of my parents have been to Europe before! It's also my first time flying without my parents. I'm very excited about this trip, but also a little bit anxious. Europe is very exciting, but also is sure to be unlike anything I've experienced before. 

My last international trip was about a decade ago: traveling to Jamaica with my parents. Air travel has dramatically changed since then, so there's a lot going on that I've either forgotten about or never seen.

My layovers in Atlanta and Amsterdam are both very brief, so I'll have to rush through passport control when already beginning to feel jet-lagged. But the jet-lag will be worst in Vienna; I arrive around 9am with my body feeling like it's approximately 2am. That's going to be tough.

### "What can go wrong, will go wrong."
In a perfect example of Murphy's Law, one of my shoelaces broke while putting my boots on after security. Hopefully that's the only thing that will go wrong today, but with roughly 18 hours of travel left ahead of me there's plenty of time left for things to go wrong. 

Here's to hoping that the transatlantic leg has some good movies!