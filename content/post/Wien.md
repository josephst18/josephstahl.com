+++
date = "2015-02-02T20:41:11-06:00"
draft = false
title = "Wien!"
categories = [ "Vienna" ]

+++

![Vienna](https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Schloss_Sch%C3%B6nbrunn_Wien_2014_%28Zuschnitt_2%29.jpg/1024px-Schloss_Sch%C3%B6nbrunn_Wien_2014_%28Zuschnitt_2%29.jpg)

I got exciting news today!  I'll be studying abroad in Vienna, Austria in May 2015.  <!--more--> München (Munich) is only 4 hours by train, so I'll also be able to visit my favorite German city!

I'll try and write about my time there, if not during my trip then after it.  The flight is about 14-15 hours (including layovers), so I'll have plenty of time to turn notes and pictures into blog posts.  The program's official title is *Origins of Modernism:  Vienna 1750 to the Present*; I'm already looking forward to taking a break from the STEM focus of a computer science and medicine curriculum.  



Picture credit:  [Thomas Wolf](http://www.foto-tw.de/)