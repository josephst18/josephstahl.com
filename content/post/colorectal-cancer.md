+++
date = "2014-08-14T23:40:16-06:00"
draft = false
title = "Retrospective Analysis of Colorectal Cancer"
categories = [ "Medicine" ]
+++

## Summer's End
On August 5, I finished my research at the University of Alabama at Birmingham (UAB) in the Department of Surgical Oncology.  While Dr. Sushanth Reddy (who directed the work) analyzes and reports the data, I can discuss my part of the study:  collecting data.  [HIPAA](https://en.wikipedia.org/wiki/Health_Insurance_Portability_and_Accountability_Act) prevents me from discussing specifics about patients, but I can still provide a summary of my work this summer. <!--more-->

I worked for **Dr. Sushanth Reddy** on an IRB-approved retrospective analysis of colorectal cancer.  Specifically, this was a study of the surgical treatment of adenocarcinomas of the left and right colon and the rectum.  I received two lists of patients with presumed colorectal cancers and surgical treatment totaling about 2400 patients.  Many patients were unsuitable for the study because of cancers outside the study's scope or because their cancer occurred in the distant past, outside the dates approved for the study.  Patients with cancers excluded from the study typically had either a squamous cell cancer or a cancer of a nearby organ (small intestine, ovary, prostate, etc) that metastasized to the colon.  After removing unsuitable patients, 2140 patients were available for study.  Of those, 861 had stage 2 or 3 colorectal cancers (as staged per [NCCN](http://www.nccn.org/) guidelines).  I expect these patients to be the most suitable for the study as patients with stage 1 cancers have very low recurrence rates which makes determining the cause (or prevention) of a recurrence difficult.  Stage 4 cancers have very high recurrence rates and therefore complicate analysis.

With data collection over, Dr. Reddy will take over the rest of the study—data analysis, writing, and publishing—while I return to Vanderbilt.

### A Study of Performing a Study
Before the study could begin, UAB required me to receive HIPAA training to access the medical records of the patients eligible for the study.  With HIPAA clearance, I received my login for Horizon, the electronic medical record system used at UAB until recently.  

Data were entered into a database I created in Microsoft Access.  A research nurse provided a list of subjects in an Excel spreadsheet which I imported into the database.  After import, the full name and Medical Record Number (MRN) of each patient was available for looking up in Horizon.  

#### Time
Looking up records on Horizon consumed (by far) the most time of any part of the study.  Because of the design of Horizon, there is no way to view all records at once, nor to view only certain categories from each record.  A diagnosis may be under the "Impressions" section of a CT scan for one patient, while another patient's diagnosis may be part of a colonoscopy report.  All information required manual collection; a query of the database to find (for example) a date of cancer diagnosis was impossible due to differences in labeling this event.  

### Retrospective

The requirement for reading ~50% of each patient's charts provided an excellent opportunity for learning about other health issues.  Diabetic patients had charts from pathology detailing toe and foot amputations.  Older patients came into clinic after falls or heart problems.  Chemo regimens varied according to a patient's age and performance status.  I looked up diseases, anatomic details, and symptoms far less frequently towards the end of the study as I learned the definitions of various terms used to describe the pathology of a cancer, the history of a patient, or the risks of a recurrence.



## Followup
I'll make sure to post a link to whatever study Dr. Reddy publishes.  Until then, I hope that some interesting conclusions can be drawn from the treatment of 2140 patients.