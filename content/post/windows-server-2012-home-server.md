+++
date = "2014-05-24T23:41:09-06:00"
draft = false
title = "Thoughts on Windows Server 2012 R2 as a home server"
categories = [ "Windows", "Technology" ]
+++

For several months now I have had an old desktop computer sitting in the back of my study waiting for a decent use.  For a while, it ran Arch Linux, but I decided to find a new purpose-built server distro because the system was difficult to keep up-to-date while I was away at Vanderbilt.  

## Background & Installation
Because I had access to Microsoft DreamSpark through school, I decided to try the newest Windows Server (2012 R2) on the old computer.  <!--more--> I transferred the ISO to a USB drive using PenDriveLinux's Universal USB Installer and installation began after several minutes spent messing with BIOS settings (after clearing the CMOS to actually enter the BIOS).  Installation was painless as far as server installations go, with a straightforward GUI accompanying.  After answering a few questions (domain, administrator login, etc.) the installer rebooted a few times and logged me in for the first time.

## Out of box experience
Unlike Ubuntu Server or Arch Linux, Windows Server boots up to a desktop (**not** the Start screen) very much like Windows 8.1.  Unlike Windows 8.1, Windows Server does not install any drivers automatically.  This meant that despite booting into a GUI by default, there was no way to set the resolution to match my display.  Initial functionality is very limited until setup is completed via the server control panel, which autostarts on first login.  Here is where new users are added, folder permissions are managed, and Storage Spaces can be configured.  A variety of other tools also exist here, although they tend to be unrelated to the tasks expected of a home server.  

## Configuration as a home server
Because this is Windows Server and **not** Windows *Home* Server, there is very little in the way of home server software set up by default; notably, DLNA sharing and public SMB shares are both absent.  SMB shares can be set up rather easily through the GUI control panel, but I found no obvious way to make a share R/W to guests.  Instead, folders have their permissions managed according to users created on the server.

Creating users on the server is a complicated process because of the default password security settings; passwords require at least three of the following to be present:  UPPERCASE, lowercase, symbols, and numbers.  Secure, but also complicated, especially for a low-security home server (these settings can be changed for more ease and less security).  Once a user is created, folders can be marked as not accessible, read, or read/write on a per-user basis.  

Here is the major shortcoming of Windows Server:  users are managed by the server rather than on individual computers.  In other words, existing (local) user accounts on individuals' computers cannot be granted permission to access folders on the server; only users created on the server (i.e. domain users) can be granted folder permissions.  In a large network of 100+ users with company-issued computers, creating and managing users on the server makes sense.  In a home, having to create new users to replace user accounts that already exist on personal computers is hardly convenient.  

Domain users are added to computers by downloading and installing software hosted by the server.  The install process is surprisingly long and requires a reboot.  After the reboot, the computer will be added to the domain and domain logins can be used.  Software to manage the server is also installed, allowing users to view their Shared Folders and allowing administrators to access the server's configuration GUI.  

Logging in with a domain login is not required to actually access folders shared with a certain domain user; however, if using a local sign-in, a user will be prompted for a username and password before accessing the shared folder.  

### Remote access
Windows Server comes with a built in remote access configuration tool (for accessing the server outside of the LAN) but I did not test it due to the difficulty of setting it up without its auto-setup (which only works with a few select DNS services).  This also meant that I could not test VPN access, although configuration certainly looks easier than OpenVPN.  

### Backup
Backup abilities built in to Windows Server are undoubtedly the killer function of Windows Server for home use.  Once backup is set up on the server, installing the companion software on any computer to connect it to the server will also set up the computer to back up to the server.  An administrator can choose what times backup will occur and whether a computer should wake up (if asleep) to make sure backup occurs.

In turn, the server can also be backed up onto another hard drive, or the disks can be mirrored via Storage Spaces to protect data loss due to drive failure.  

### The other features
Looking at the other tools that Windows Server comes with is a sure sign that Window Server is not meant for home use.  Things like Exchange integration, Office 365 integration, and a DNS server are all useful for a corporate environment but completely redundant in a home environment.  

## Conclusion
I have no doubt that Windows Server 2012 R2 is excellent for certain server environments, especially on networks with large amounts of Windows computers.  However, it is (as younwould expect) unsuited for home use.  The features I would expect to use heavily (DLNA and public folder sharing) are completely absent, while other features are more difficult than they should be for home use (remote access and folder permissions).  Some features, especially backup, are excellently done but are not worth the difficulty of configuring a domain with users and designating folders.  So while Server 2012 R2 is undoubtedly an excellent server OS, it is very difficult to use as decent home server OS.

Now the computer runs Ubuntu Server with Ajenti for a remote-access control panel, Samba is configured with a public share for XBMC to browse and users to add media to, Plex for sharing media to a Roku, and an XFCE desktop because I'm not quite ready to give up entirely on the excellent GUI configuration of Windows Server.  