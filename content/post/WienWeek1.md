+++
categories = [ "Vienna" ]
date = "2015-06-09T16:16:04-05:00"
description = "After an unexpected detour through Basel, I finally arrived in Vienna! My first week was a whirlwind of eating Austrian food, visiting museums, and countless amazing views of Wien."
keywords = [ "article", "Vienna", "Austria", "Travel", "Europe"]
title = "Vienna, Week 1"
leadpicture = "https://josephstahl.com/media/WienWeek1/header.jpg"
draft = false

+++

![Staatsoper](/media/WienWeek1/header.jpg)
*Staatsoper, one of the my first sights in Vienna*

I'm back home in the United States, which means I finally have the time to write up my trip to Vienna! I'll start at the Atlanta airport (where I published my previous post). I'll make sure to write about the rest of my trip later; for now, here are the notable moments of my first week.

<!--more--> 

## The rest of the flight
First of all, I spent much more time in the Atlanta airport than I planned. My Delta flight was delayed approximately 1 hour 40 minutes, leaving my connection in Amsterdam uncertain. The flight was largely uneventful; I had reserved an aisle seat which proved to be an excellent decision. The flight felt much longer than the ~9 hours it actually was because most of the flight was at nighttime, thanks to flying east. 

By the time I landed in Amsterdam, my connection to Vienna had already departed. After talking with a KLM agent, I learned that all flights to Vienna later that same day were already booked, so I was rebooked onto a flight to Basel, Switzerland, and from there to Vienna. 

![Arrival in Schiphol Airport](/media/WienWeek1/WienWeek1-1.jpg)
*Amsterdam Schiphol airport*

After this detour, I finally made it to Vienna—several hours after planned but nevertheless there. To my (pleasant) surprise, my checked bag had followed me through the detour. It took three trips through security, three passport checks (and one stamp), and one extra flight, but I was finally in Austria at 3pm local time. However, it felt like 8am to me so I was growing tired after not sleeping on any of my flights.

I ended my day by going with Professor Zeller (who had previously picked me up at the airport) to purchase an Austrian SIM card for my phone and then got dinner at Cafe Florianhof. 

## May 5
After everyone arrived the previous day, this Tuesday marked the official beginning of class. We met in the hotel lobby for breakfast (excellent and complimentary Austrian breakfast) and then rode the subway system to the [Stattsoper](https://en.wikipedia.org/wiki/Vienna_State_Opera) where we began a bus tour of Vienna. The bus tour took us around the Innere Stadt with a brief trip across the Danube to see the UN complex and Donau City skyscrapers. Back at the [Donaukanal](https://en.wikipedia.org/wiki/Donaukanal) (built to control flooding) we transferred to another tour bus to see the sights along the Donau River. 

After lunch, the class met for a tour of the Austrian Parliament building, which has been rebuilt from Allied bombing in WWII and again serves as the home of the Austrian government. 

I saw an interesting structure while running, a [flak tower](https://en.wikipedia.org/wiki/Flak_tower) built by the Nazis that still stands in one of the parks in Vienna. I later learned that it is not there for historical value; rather, the tower remains because it is too large to demolish. The tower was built to withstand direct bombing, which means that gathering enough explosives to demolish it would likely harm or destroy many of the surrounding buildings. 

![Austrian Parliament](/media/WienWeek1/WienWeek1-2.jpg)
*Austrian Parliament building*

### Vienna's public transport
The public transport of Vienna deserves a special mention here for how reliable, efficient, and useful it is. The system is divided into four parts: U-Bahn, S-Bahn, trams, and buses. The U/S-Bahn lines run underground in the very center of Vienna (the Innere Stadt) but move above-ground in the outer districts and suburbs. 

I did not ride the bus system much, but I rode the U & S-Bahn system and trams everyday. I never heard of or experienced a breakdown on any of the lines, nor were the estimated times posted ever inaccurate. 

Getting onto the trams/ trains is also different than the United States; instead of swiping a ticket to go through a turnstile, one can get onto public transport without ever showing a ticket. However, there are plainclothes ticket inspectors that ride the subway and trams and will issue severe fines to "black riders": people riding without a valid pass. 

* A [map (PDF) of the public transport system](http://www.wienerlinien.at/media/files/2014/svp_2013_106483.pdf), for the curious

## May 6
The day started with a tour of the [Jewish Museum](http://www.jmw.at/en/) in Vienna, a museum which is dedicated to all Jewish history in Vienna rather than exclusively the Jewish victims of the Holocaust. The history of Judaism in Vienna has been a troubled one, evidenced by the fact that the city is now in its fourth "Jewish settlement" after three previous expulsions of Jewish people.

In the afternoon, we visited the [Museum of the Romans](http://www.wien.info/en/sightseeing/sights/from-l-to-r/roman-museum), an excellent although small museum that focuses on the Roman founding of Vindobona—the settlement that eventually became Vienna. Afterwards we headed to the Albertina, a state-owned art museum housing an impressive collection of works by da Vinci, Duerer, Bruegel, Monet, and others. 


## May 7
The class met at the [Naschmarkt](https://en.wikipedia.org/wiki/Naschmarkt) for lunch, Vienna's most famous outdoor market. The variety there is incredible, with everything from falafal stands to wine bars and produce stores to gelato shops. We then rode the U-Bahn to [Schönbrunn Palace](http://www.wien.info/en/sightseeing/sights/imperial/schoenbrunn-palace), which has a (by palace standards) rather plain front but magnificent interior. It used to be the summer residence of the Habsburgs but is now a museum and public garden. The public garden behind the building is incredible—filled with statues, fountains, orchards, sculpted bushes, and a hill with a view of all of Vienna.

![Schoenbrunn Palace](/media/WienWeek1/WienWeek1-3.jpg)
*Schönbrunn Palace*

## May 8
Keeping the trend of palace visits going, we visited the [Upper Belvedere Palace](http://www.belvedere.at/en) today, a building originally constructed for Prince Eugene, a highly successful military commander. The palace today houses the Belvedere Museum, an art museum which includes Gustav Klimt's *The Kiss* and works by Egon Schile as well as landscapes and modern art. 

We again ate at the Naschmarkt, which gave me a chance to visit an Apotheke, the Austrian equivalent of a pharmacy, to get medication for a sinus infection. Apothekes in Austria are ubiquitous because virtually no medication can be bought off-the-shelf. Even ibuprofen must be purchased over-the-counter at an apotheke. 

After lunch we toured the nearby [Sezessionshaus](https://en.wikipedia.org/wiki/Secession_Building,_Vienna), the headquarters of the Austrian Secession. The main exhibition area of the building is still in use today for art exhibitions while a room in the basement has [Klimt's *Beethovenfries*](https://en.wikipedia.org/wiki/Beethoven_Frieze) on permanent display. 

![Secession House](/media/WienWeek1/WienWeek1-4.jpg)
*Secession Building*

That evening, I had standing tickets to a Barenboim piano recital at the [Musikverein](https://www.musikverein.at/), the city music hall. Both the performance and the venue were spectacular.

## May 9-10
This weekend I worked with Brandon (my roommate) on an assignment from Professor Zeller: city district research. We were assigned [Favoriten](https://en.wikipedia.org/wiki/Favoriten), the 10th district of Vienna and a district notorious for having nothing interesting at all. During lunch on Saturday in Favoriten, I asked our waitress what there was to do in Vienna; she replied with a prompt "gar nichts"—totally nothing. 

The next day, Sunday, was one of my most memorable in Vienna. I set out to get lunch, but ended up going on a much larger adventure. I walked to the Naschmarkt but all the restaurants were closed—my first experience with Vienna's public schedule, where all shops close around 7pm on weekdays and stay closed all day Sunday. Eventually I got a quick meal at a bakery, then walked across the Donaukanal and then the Danube to visit [Donau City](https://en.wikipedia.org/wiki/Donau_City) (looked around the skyscrapers there) and the UN complex. From there I took a subway to Heiligenstadt, the northernmost subway stop and the beginning of the vineyards of Vienna.

![Mexikokirche](/media/WienWeek1/WienWeek1-5.jpg) 
*The [Mexikokirche](https://en.wikipedia.org/wiki/St._Francis_of_Assisi_Church,_Vienna)*

I kept walking north and eventually got to my first vineyard where I found a path and kept walking up the hill it was planted on. After a surprisingly steep climb to the top, I was rewarded with a view of all of Vienna, this time from the north (while at Schönbrunn Palace I had been looking from the south). I walked along the road and passed several heurigen—famous parts of Viennese culture serving the current year's vintage from an associated vineyard—then headed back towards Vienna as the sun set. 

![The hills of Heiligenstadt](/media/WienWeek1/WienWeek1-6.jpg)
*Looking out over Vienna, with the UN and Donau City on the left and Innere Stadt on the right*

----------
## General thoughts
- ATMs: ATMs, called Bankomats in Germany and Austria, are widespread and free. Which is good, since many restaurants, stores, and stands are cash-only.
- Air conditioning: Rare in private homes and hotels, although slowly becoming more common. More frequently found in stores and office buildings.
- Etiquette: Austrians are very quiet, including on subways where the majority of people bring a phone or newspaper and pass their time without talking. 
- Dining: Waitstaff in Austria will not bring the check until you ask for it, which is a blessing when enjoying one's time at a cafe but can be terribly inconvenient when one is in a rush to get somewhere.