+++
categories = ["Vienna"]
date = "2015-06-11T18:33:10-05:00"
description = "I studied church architecture, visited a monastery, took a cruise on the Danube past centuries-old castles, and bought chocolate. Yep, a normal week in Vienna!"
keywords = ["article", "Vienna", "Austria", "Travel", "Europe"]
leadpicture = "https://josephstahl.com/media/WienWeek2/heropicture.jpg"
title = "Vienna, Week 2"
draft = false

+++

![Fountains at the Vienna UN](/media/WienWeek2/heropicture.jpg)
*The fountains at the Vienna United Nations, with flags in the background*

I studied church architecture, visited a monastery, took a cruise on the Danube past centuries-old castles, and bought chocolate. Yep, a normal week in Vienna!  
*Note: this is a continuation of the previous post detailing my 3.5 weeks studying abroad in Vienna. This post finishes with May 14; May 15-18 were spent in Munich and will get their own post!*

<!--more-->

## May 11
Following city district presentations, the class gathered to tour modernist architecture in Vienna. First was the [Adolf Loos Haus](https://de.wikipedia.org/wiki/Looshaus), then the [Kirche am Steinhof](https://en.wikipedia.org/wiki/Kirche_am_Steinhof) and the [Postparkasse](http://www.greatbuildings.com/buildings/Post_Office_Savings_Bank.html), both designed by Otto Wagner. The setting of the Kirche am Steinhof is fascinating; the church is part of a much larger mental health facility that is still in use today. At the time of its construction, the facility was remarkable for allowing patients to come and go as they pleased instead of adopting the "lock them up" attitude that prevailed. 

![](/media/WienWeek2/WienWeek2-1.jpg)
*Otto Wagner's Kirche am Steinhof*

That evening I visited Cafe Central, first opened 1876 and one of Vienna's most well-known cafes. The cafe's menu boasts of its literary history, being frequented by Lenin and Trotsky, among others. It's [Wikipedia page](https://en.wikipedia.org/wiki/Caf%C3%A9_Central) offers up a bit of history that Cafe Central understandably leaves off the menu: the cafe also served Adolf Hitler. The cafe is deservedly well-known for its apple strudel, which is accompanied most nights by a pianist playing the relaxing background music that one would expect to find in a classic Viennese cafe. 

## May 12
May 12th started early, with passports out and a trip to the [UN complex](https://www.unvienna.org/). Because of an agreement with Austria, the UN building is not technically on Austrian lands; rather, the entire complex is considered international territory. For the people who work there, this means they can enjoy duty free shopping. For students, all this means is that passports are needed for entrance (also for security screening). Once in, a tour guide showed us around the building before depositing us at the IAEA for a lecture on atomic weapons and "atoms for peace"—the phrase coined by Eisenhower that refers to the use of nuclear technology for peaceful purposes such as energy and medicine. 

![](/media/WienWeek2/WienWeek2-2.jpg)
*A briefing room in the UN building*

That afternoon, the class visited the Kahlenburg house, one of the first minimalist buildings built in Vienna. This building, in contrast with Secessionists' modernist architecture, eschewed all decorations. It was a clean white building with sharp 90-degree edges and few windows.

Because class ended relatively early, I visited a grocery shop called [Julius Meinl am Grabben ](http://www.meinlamgraben.at/Home) to buy gifts. The store is incredible; it is far larger than any other grocery store I visited in Vienna (two floors!) with a vast selection of wine, chocolate, and jams. Every item's county of origin is marked by a flag on the price tag, making buying Austrian souvenirs easy. Meinl's coffee is remarkable in that many cafes in (and outside of) Vienna serve their coffee. It's great! 

## May 13
Class started early again today, this time because of a trip to [Melk Monastery](https://en.wikipedia.org/wiki/Melk_Abbey). The monastery is, as the name implies, in the town of Melk, located west of Vienna and along the Danube. We rode a two-level train there—my first experience with European rail! The monastery is massive and located on a large hill overlooking the town and Danube. At one point it was exclusively a monastery (less the wing that was reserved for Habsburg use while traveling) but today serves as a monastery, museum, and Catholic school. The massive library there consists of twelve rooms, each filled with historical books covering both theology and science. The chapel cathedral is equally impressive, with a magnificent pipe organ and gilded surfaces everywhere. 

![](/media/WienWeek2/WienWeek2-3.jpg)
*The organ at Melk Monastery*

The trip back from Melk was likewise interesting; we took a river cruise to [Krems](http://wikitravel.org/en/Krems) then a train back to Vienna. The river cruise brought us past an assortment of castles that used to (forcefully) collect taxes from passing ships during a less orderly time in Vienna's history. 

Once back in Vienna, there were a few minutes to change clothes before we again departed, this time to the Musikverein to see the Viennese Philharmonic (in Deutsche, the *Wiener Symfonique*) perform two Sibelius pieces, a Schubert cello concerto, and a Haydn piece. 

## May 14
![](/media/WienWeek2/WienWeek2-4.jpg)
*One of many statues in the Museums Quartier*

May 14th was spent in the Museums Quartier, a part of Vienna located very close the Hofburg Palace. In fact, two of the largest museums there, the Art History and Natural History, were built by Emperor Franz Josef I to house the Habsburg collections. First on the class schedule was the [Art History museum](https://en.wikipedia.org/wiki/Kunsthistorisches_Museum) (the *Kunsthistoriches Museum*) which houses several Vermeer paintings, among other famous masters. A tour of the royal treasury followed, where the crown jewels, coronation attire, cloaks, baptismal gowns, and various other royal paraphernalia are housed alongside a vast collection of precious stones and jewels. 

After lunch the class met again to tour the [Natural History museum](https://en.wikipedia.org/wiki/Naturhistorisches_Museum) (*Naturhistorisches Museum*), which claims bragging rights for the largest mineral collection in the world. The collection is far less boring than one might expect from a rock collection; on display are several large gems, a massive lump of gold, and meteorites including the one that fell in Russia in 2013. The second floor holds animals and plants, including several now-extinct animals. 