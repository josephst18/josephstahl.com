+++
date = "2014-03-22T23:41:22-06:00"
draft = false
title = "PPTPD Configuration"
categories = [ "Technology", "Linux" ]
+++

## Background
Although OpenVPN is the most secure VPN server in common use, I find it very difficult to setup, and not really worth the effort for protecting relatively unimportant communications.  I've gone to using PPTPD instead to create a VPN for protecting my communications while on untrusted wifi networks.<!--more-->  

PPTPD is much easier to configure than OpenVPN because it does not need any of the key setup (less secure but more convenient).  I've taken to referencing the Arch Linux wiki's concise and understandable [PPTP Server](https://wiki.archlinux.org/index.php/PPTP_Server) page for instructions on configuring PPTPD.  When I set up the server (on the $5/month DigitalOcean droplet), I followed the instructions very precisely, so I was puzzled as to why the server would start without complaint but refuse all connections.

## Troubleshooting
Fixing the problem took a good part of a Sunday because I kept on expecting the problem to be in `/etc/ppp/pptpd-options` while the problem actually existed in `/etc/ppp/chap-secrets`.  I had tried adding a user called `test` to the server with password `simplepass` and I was able to connect without issue.  Example: 

	<username>	<service>	<password>		<ip>
    test		pptpd		simplepass		*
    joseph  	pptpd		S3CuRE*PasSW0RD	*

The login for user `test` worked but `joseph` did not.

## Solution and lessons learned
NEVER put asterisks in a file where asterisks are wildcards.  Seems obvious now, but when there were &, %, $, and other special characters, a * didn't seem out of place.  However, PPTPD was reading the file, coming to the asterisk, and treating only the part of the password before the asterisk was my password (presumably the asterisk was interpreted as the IP address to assign and the characters after the asterisk were discarded).  Replacing my password with an alphanumeric password immediately fixed the problem and my server has been running smoothly since.

Well, running smoothly until I wiped it to have a clean slate for my Ghost migration.  Good thing PPTPD's easy to set up!