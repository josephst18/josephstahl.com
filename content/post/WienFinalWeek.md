+++
categories = ["Vienna"]
date = "2015-07-09T17:05:24-05:00"
description = "I spent my final week in Vienna visiting cemeteries, eating cake, and watching \"horse ballet\". And oddly enough, these are all important parts of Viennese culture!"
draft = false
keywords = ["article", "Vienna", "Austria", "Travel", "Europe"]
leadpicture = "https://josephstahl.com/media/WienFinalWeek/WienFinalWeek-1.jpg"
title = "Vienna, the final week"

+++

![Graves at the Zentralfriedhof](/media/WienFinalWeek/WienFinalWeek-1.jpg)
*Graves at the Zentralfriedhof*

I spent my final week in Vienna visiting cemeteries, eating cake, and watching "horse ballet". And oddly enough, these are all important parts of Viennese culture!
<!--more-->

## Friday (May 22)

In the morning I went on perhaps my most unusual trip yet with the class: a tour of the Zentralfriedhof (Central Cemetery). The cemetery, despite its name, is far outside the city, requiring about a 30 minute tram ride to reach the entrance. At the time of its construction people were understandably angry with the location of the cemetery; it was located so far outside Vienna that one had to take a lengthy carriage ride just to visit a grave. Even today, the cemetery is located far outside the "heart" of Vienna and is surrounded by much newer (and unremarkable) buildings. 

The cemetery rivals Arlington in size; it took me about 10 minutes to walk half the length of the cemetery at a fast walking pace. Religions are kept separate in the cemetery, with sections for each religion and new sections being occasionally added, such as the Buddhist section. Perhaps all of Vienna's history and culture could be told through the Zentralfriedhof. The cemetery is an excellent reminder of the "death culture" that permeates Vienna and also the resting place of some of Vienna's most famous residents: Beethoven, Brahms, Strauss, and Schubert. 

After an hour spent at the cemetery, the class traveled back to Schwedenplatz for lunch and then north to the Karl-Marx-Hof, a 1.1km long public-housing building that I had unknowingly walked past a week and a half ago on my trip to the vineyards. Public housing in Vienna carries none of the stigma that it does in the United States as is apparent when touring the building. Everything is in good condition and the entire area feels safe; in fact, getting into socialized housing can be a highly competitive process due to demand. 

## Saturday
With no class on Saturday, I was free to see Vienna on my own. Before going to Vienna I purchased tickets to the Spanish Riding School and so I headed downtown to the Hapsburg Palace to see the famous white Lippizaner horses perform. The show builds up to its finale: first, the young horses (who have not yet developed their all-white coat) perform, then the fully trained Lippizaners perform ground work, then the show concludes with "above ground" work with pirouettes and jumps.

![Spanische Hochreitschule](/media/WienFinalWeek/WienFinalWeek-2.jpg)
*Young Lippizaners at the Spanish Riding School*

After the show I made another trip to Julius Meinl am Graben, this time to purchase apricot jam and pumpkin seed oil—two Austrian specialties I had forgotten before. That evening was the Eurovision Song Contest, but with the city exceptionally crowded and rain falling, I decided to stay in to watch it. 

## Sunday
Sunday morning was spent as all Sunday mornings should be: enjoying a lazy morning. But around noon I went for a run and showered before heading to Kurkonditorei Oberlaa to try a Sacher Torte, a flourless chocolate cake with apricot jam and cream. I have decided that despite being the most well-known cake in Vienna the Sacher Torte falls short of the Esterhazy Torte. For being a chocolate cake it is surprisingly short on chocolate flavor. 

![Sacher Torte](/media/WienFinalWeek/WienFinalWeek-3.jpg)
*The Sacher Torte at Kurkonditorei Oberlaa*

## Monday
We took a guided tour of the House of Music on Monday morning. The "House of Music" is an interactive museum/ exhibit-space devoted to music, the production of music from classical to modern, and how humans perceive music. After lunch we continued our musical themed day by visiting the Mozart House, the only preserved residence of Mozart remaining in Vienna. The building is divided into two parts, a museum portion chronicling the life of Mozart in Vienna, and his actual apartment with explanations of the function of each room. 

After these two tours we finally had a chance to climb the Stephansdom spire. The view, of Stephansplatz and the Innere Stadt, is nice although I enjoyed the view from the Budapest dome more because I could walk around outside. Still, the view from the top justifies the long climb up.

![View from Stephansdom](/media/WienFinalWeek/WienFinalWeek-4.jpg)
*The roofs and towers of Vienna, as seen from the Stephansdom spire*

## Tuesday
Tuesday was perhaps the least busy day of the trip although also one of the best: it was café day! Vienna has a reputation for its café culture, so today we had the chance to visit an historic café and a more modern one. Part of café culture dictates that one sits and enjoys her or his time at the café; this is apparent in the fact that no self-respecting café would ever offer coffee to-go. Instead, patrons sit and either read the newspapers and magazines provided or sit and enjoy conversation with others. 

At the second café I had the Esterhazy Torte, which I previously called out for being the best cake I had in Vienna. The Esterhazy is a simple cake with pistachio icing and despite its lack of ornamentation is delicious. 

![Esterhazy Torte](/media/WienFinalWeek/WienFinalWeek-5.jpg)
*The Esterhazy Torte*

That evening Professor Zeller and Laura were taking us all out to dinner to celebrate the (almost) end of our program so the class headed back to the hotel to change. We left as a group for the Greichbeisl restaurant where I was happy to learn there was free bread, but disappointed to learn there was no free water. I decided to have one of my last "100% Austrian" meals so I ordered the asparagus soup and veal Wiener Schnitzel. I could write an entire article about asparagus in Germany and Austria, but the fact that restaurants have entire menus devoted to asparagus dishes (all white asparagus!) should portray some of the national obsession with asparagus. 

After dinner we ran into a typical problem: splitting the bill. This time was especially complicated because each person was paying for their own drinks but Professor Zeller (aka Vanderbilt) was paying our food tab. And with waiters refusing to split the bill (despite being asked to, prior to the meal) no one was sure what their bill was supposed to be. 

## Wednesday

![View from the Alps](/media/WienFinalWeek/WienFinalWeek-6.jpg)
*A small village below the Austrian Alps*

Wednesday morning started early as the class rode a train to the Alps. Once there we took a cable car up the mountain to begin our hike; however, no one was prepared for the weather, which had become far colder, windier, and ice-storm-ier than it had been in Vienna. About a mile into the hike Professor Zeller decided to drastically shorten the hike so that we could avoid any risk of hypothermia, especially if it started raining. 

The view from the Alps, even on an overcast day, was incredible, with huge mountains and small villages in valleys surrounding me on every side. Because the very top of the mountain was above the tree line, it was even windier than at the top cable car station and I soon made my way back down the mountain and to the much warmer valley. 

## Thursday
Thursday marked my final day in Vienna. I woke up at 6:30am to take the airport bus and leave my favorite European city behind. I got to the Vienna airport and realized I did not remember any of the layout; I suppose this was a result of being so severely jet lagged the first time I got there. After following some signs around I figured out where to check in and was relieved to learn that (despite bringing two bottles of wine home) my bag was under the 50lb limit. I first flew to Paris, whose airport was much less confusing than I had been warned, then back to Atlanta and finally to Birmingham, getting home around 9pm local time. 

During my transatlantic leg, I got an offer from a flight attendant to move up to an empty row on the flight, so while I was still in economy class I had no seats on either side of me and about six feet of leg room in front of me. I was also the first off the flight in Atlanta, which allowed me to get through customs far faster than I expected. So despite their unresponsive entertainment systems, Air France is by far my favorite airline. 

## Final thoughts

![Schönbrunn Palace](/media/WienFinalWeek/WienFinalWeek-7.jpg)

- Studying abroad was an incredible experience and I would recommend it to anyone.
- Vienna is a fantastically safe city. I never felt like I was in an unsafe area.
- Culture shock is a real thing. For example, when I first got to Vienna I couldn't figure out why things closed so early or why people didn't tear down the buildings to build larger ones. 
- Reverse culture shock is real too. I noticed this as soon as I got to the Paris airport and was waiting for my flight to Atlanta. After spending a month in Vienna, a normal conversation between Americans sounded painfully loud.
- Transportation in America is far worse than in Austria. Roads there are better, public transport there is far more extensive, and the rail system makes America's look laughable. Austria and Vienna have every level of transportation covered, from intra-city travel by high-speed rail all the way to local travel with buses and trams. 
- Air condition is difficult to find in Vienna. It is almost non-existent in private buildings and still surprisingly uncommon in public ones. 
- A 21 year-old drinking age in the United States feels (even more) ridiculous after spending time in Europe. 
- American culture feels much less patient compared to European culture. Stores here are open 7 days a week for long hours and people want things **now**. In Europe stores are open for much shorter hours and waiting is not just tolerated but enjoyed. This is most apparent in café culture, where Americans expect fast coffee to-go and the Viennese sit down with their coffee, often for several hours. 