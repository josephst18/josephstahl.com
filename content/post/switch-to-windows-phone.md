+++
date = "2014-07-14T23:40:38-06:00"
draft = false
title = "Making the Switch: Windows Phone"
categories = [ "Technology", "Windows" ]
+++

*Since Windows Phone is still a relatively unheard-of mobile OS when compared to the two giants, I decided to write about my experience switching in the hope of making a switch to Windows Phone less unfamiliar to others.* <!--more-->

## Preface
For the past 24 months, I have used a Samsung Galaxy Nexus.  While the phone came with one distinct advantage over other Verizon phones (bootloader unlock), it also came with an array of annoyances, deficiencies, and shortcomings that became more and more prominent as the phone aged.  Battery life, photo quality (front and back cameras), and deterioration of apparent speed during everyday use combined to make the phone progressively less fun to own.  

One of the greatest issues, however, was the problem the Nexus line was supposed to fix.  When Google announced an update, it was supposed to come to all Nexus devices almost immediately.  The Verizon Galaxy Nexus become an exception to this, instead getting the update after a lengthy delay imposed by Verizon; Google seemed to accept this and never sidestepped Verizon by posting factory images simultaneous with the carrier-unlocked GSM model of the Galaxy Nexus.  Even today, the [Factory Images for Nexus Devices](https://developers.google.com/android/nexus/images) website lists an Android 4.3 image for the GSM model of the Galaxy Nexus (maguro) while only offering 4.2.2 for the CDMA Galaxy Nexus (toro).

There have always been methods to update *almost* simultaneous with other users, typically by relying on a ported factory image and later switching to unofficially built `update.zip` files that could be flashed in recovery mode (albeit with an unofficial recovery ROM).  Both methods are as buggy as they sound, considering neither has Google's blessing nor bug squishing abilities.

After dealing with 24 months of alternating between stable but out-of-date and current but buggy releases, I decided that my next phone would **not** be an Android phone.

## Why Windows Phone?
*Disclaimer:  my opinions of Windows Phone are based on my experience with the 8.1 Developer Preview, which is a [drastic](http://www.windowsphone.com/en-us/features-8-1) upgrade over 8*

My choice of Windows Phone is a personal one and certainly not based on my opinion that Windows Phone is objectively a better platform.  In fact, I am reasonably sure that iOS 8 is in most ways a superior mobile OS to Windows Phone 8.1 (having not used iOS 8, I cannot say for sure).  

That said, I would happily choose Windows Phone again.  I agree with [Jay Machalani](http://jaymachalani.com/blog/2014/5/29/pushing-ios) that Windows Phone has a unique position between customization and experience—less customization than Android, but a much better experience when compared to Android on a two year old smartphone.

The fact that I use a Surface Pro 2 and a home-built desktop, both with Windows 8.1, also played into the decision.  I already rely on Outlook.com, Calendar, OneDrive, Office 365, and especially OneNote on my computers, so it makes sense to carry this over to my phone.

## The switch
On Verizon, the [Lumia Icon](http://www.nokia.com/us-en/phones/phone/lumia-icon/) (aka the Lumia 929) is unquestionably the only Windows Phone worth buying.  

So with the hardware already decided upon and ordered, the only question is migrating services.  Just like the iPhone works best with iCloud and Android works best with Google Apps, Windows Phone works best with Microsoft's cloud.  I forwarded all my `@gmail.com` mail to my `@outlook.com` address, downloaded my Google Calendar and imported it into Outlook.com's calendar, did the same with my contacts, and moved all my 2-factor auth over to Windows Phone's Authenticator app.

All in all, it took about 30 minutes, with most of that time spent visiting various websites and switching 2-factor auth.

## First impressions
On first boot, the experience was remarkable.  I have 2-factor auth set up on my Microsoft account, so I was expecting to have to enter an app-specific password to connect my phone to my Microsoft account.  Instead, I entered my normal sign-in information, the phone recognized that its number matched the mobile number attached to my Microsoft account, and authenticated itself automatically.  

I immediately upgraded the phone to Windows Phone 8.1 by registering as a developer with Microsoft (free to register).  After marking my phone as a developer phone, I downloaded [Preview for Developers](http://www.windowsphone.com/en-us/store/app/preview-for-developers/178ac8a1-6519-4a0b-960c-038393741e96) and the upgrade began.  My data remained intact during the upgrade and ~15 minutes later, my phone was on the latest version of Windows Phone.  I did not need to wait on Verizon or download a ported version from XDA; the update was provided by Microsoft and installed without issue.

## The app situation
I am not a heavy user of apps.  This said, there are a few apps that I use every day and must have on Windows Phone.  Rudy Hyun's [6snap](http://www.windowsphone.com/en-us/store/app/6snap/82fa6341-28dc-4203-bd08-9749b167bc4b) is an excellent 3rd party Snapchat client.  [Baconit](http://www.windowsphone.com/en-us/store/app/baconit/fa4e749a-a521-43f1-b2cd-40bd1a0a8875), [Nextgen Reader](http://www.windowsphone.com/en-us/store/app/nextgen-reader/643381de-4724-e011-854c-00237de2db9e), OneNote, and [HERE Maps](http://www.windowsphone.com/en-us/store/app/here-maps/6c28635b-11ae-409a-8503-b02ac71f8362) replace Reddit Sync, Feedly, Google Keep, and Google Maps, respectively.  

There are also a few apps on Windows Phone that are unique the platform.  [Office Lens](http://www.windowsphone.com/en-us/store/app/office-lens/5681f21c-f257-4d62-83f5-5341788a5077) is an enhanced document and whiteboard scanner that automatically processes images and saves them to OneNote.  [MixRadio](http://www.windowsphone.com/en-us/store/app/mixradio/f5874252-1f04-4c3f-a335-4fa3b7b85329) is a Microsoft (née Nokia) app similar to Songza and Pandora:  playlists to match mood or artist.  It also allows playlists to be downloaded for offline listening, useful for metered data plans.  Of note, MixRadio is free and ad-free, although a paid plan allows using MixRadio on Windows 8.1 and grants unlimited song skips.

Apps on Windows Phone still lag behind their iOS and Android counterparts.  Official apps from Facebook, Twitter, Foursquare, etc. exist but tend to have fewer features than the official apps of other platforms.  "Startup apps"—apps like Secret and Threes from companies that have just launched—are almost nonexistent on Windows Phone.

## Camera
*[Camera samples from Engadget](http://www.engadget.com/gallery/nokia-lumia-icon-camera-samples/).  My own pictures aren't decent tests of the camera; Engadget does a better job here.*

I am not a serious photographer, but the camera on the Lumia Icon deserves all the praise I can give.  It takes 19MP stills (although I prefer the 16MP pictures at a 16:9 aspect ratio) and 1080p/30 video.  

The Nokia Camera app includes several features not found in the standard Windows Phone Camera app:  the ability to save pictures as a `.DNG` for later processing and and the option to save a 16MP still alongside a 5MP still, making sharing easy (upload the 5MP file) while allowing reframing the photo at a later date by cropping the 16MP photo.

## Daily Use
The Lumia Icon handily beats all other phones I have owned.  Early OS releases are easy to apply, the phone runs with smoothness and fluidity that I have not seen in Android, and the battery lasts a day of 4G use with energy to spare.  

Music is the platform's second major failure (apps remain the most visible shortcoming).  Despite biweekly updates, the Xbox Music app remains unusable on Windows Phone.  Playlists are slow and sometimes impossible to scroll through.  The Windows Phone App for Desktop ignores playlists made in Xbox Music for Windows 8.1 when syncing.  Furthermore, Microsoft disallows most use of the Xbox Music app without a Xbox Music Pass.  While playlists & albums can be viewed, neither can be played or downloaded for offline listening without having an Xbox Music Pass.  If music is already on the phone, it *can* be played, but the shortcomings of the Windows Phone App for Desktop have frustrated my attempts to sync the songs of a playlist to the phone, as my playlists are not recognized by the app.

**I would choose the Lumia Icon and Windows Phone 8.1 again**, despite these shortcomings.  Recreating my playlists in MusicBee took time but simplifies syncing.  I now play my music in MixRadio, complete with quickly-loading playlists and first class offline support.  The hardware falls short of the Galaxy S5 or HTC One M8, but runs every bit as smoothly thanks to Windows Phone.  Problems with my Galaxy Nexus (airplane mode bugs, battery life, horrible 4G reception, and a 2 month long failure to receive MMS messages) are absent from the Lumia Icon.  The photo quality places the Lumia Icon above all other phones I have owned or tried.  Most importantly, the software is stable, the battery lasts >14 hours, and the phone is reliable.  My switch to Windows Phone was quick, easy, and even impressive at times.