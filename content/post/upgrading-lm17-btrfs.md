+++
date = "2014-05-02T23:40:56-06:00"
draft = false
title = "Upgrading to Linux Mint 17 with Btrfs"
categories = [ "Linux", "Technology" ]
+++

![Linux Mint Logo by Clement Lefebvre, CC-BY](https://upload.wikimedia.org/wikipedia/commons/5/5c/Linux_Mint_Official_Logo.svg)

Determining how to upgrade a Btrfs-based system without the possibility of harming my `/home` partition took me several hours of searching, so I decided it was worth sharing. <!--more-->

Arch Linux introduced me to Btrfs, a (experimental) file system with a number of improvements, especially in creating subvolumes and data compression.  When I switched to Linux Mint 16 I decided to keep Btrfs and was happy to see that the guided installer (Ubiquity-based) could automatically setup Btrfs with subvolumes given a Btrfs-formatted partition (provided the partition is already Btrfs formatted; the absence of `btrfs-tools` from the live media means that empty partitions cannot be Btrfs formatted out of the box).  Following the installation, I was left with an Ext4 boot partition and swap partition (created myself) and a large Btrfs partition with two subvolumes:  `@` (mounted to `/`) and `@home` (mounted to `/home`).

### Problems 
Separating `/` and `/home` onto separate partitions makes system upgrades simple:  preserve user data on `/home` while replacing system components on `/`, so it makes sense for installers to attempt to separate these folders onto separate partitions if given control of partitioning.  While Linux Mint's installer supports *creating* subvolumes on a Btrfs partition as part of installation, its partitioning wizard does not actually *recognize* subvolumes.  **Linux Mint's Ubiquity installer does not allow users to select subvolumes of Btrfs partitions as targets for installation.**  

### Preparing to upgrade 
While most operating systems (many Linux distros included) support in-place OS upgrades, my past experience with Windows has convinced me that fresh installations always experience less problems and the time saved by performing an in-place upgrade is typically lost in troubleshooting unusual problems related to upgrading.  **My goal:  to install [Linux Mint 17](http://linuxmint.com/) to the `@` subvolume without harming my `@home` subvolume.**

A problem is apparent here; Linux Mint can only be installed via the Ubiquity installer, which does not recognize Btrfs subvolumes (sidenote:  the absence of `btrfs-tools` likely has a lot to do with this, although installing it does not fix recognition).  Thanks to a combination of Ask Ubuntu, Stack Overflow, the Arch Wiki, and several other resources, I figured out how to upgrade without deleting any data.

### Upgrading 
Before beginning the installer, open up a console and mount the existing Btrfs partition with `@` and `@home`.  `# mount /dev/sdXY /mnt/` (where `/dev/sdXY` is the Btrfs partition) will suffice.  The Btrfs subvolumes will display as folders in the mounted partition.  Rename the `@` and `@home` folders to something memorable and unique (this will prevent the guided installer from overwriting the contents; best to prevent Ubiquity from messing with your home folder).  Unmount the Btrfs partition and begin the installer.

When the Linux Mint installer reaches the partitioning screen, setup partitions as per your preferences.  The `/boot` partition can be formatted, but the Btrfs partition **must not** be formatted.  Just select it as the `/` mountpoint and continue with the installer.  Keep the username the same as the existing user to prevent headaches with adding/ deleting users and managing `/home` folder permissions.  When installation is complete, close the installer but **do not** reboot.  

Instead, open another console and remount the Btrfs partition as you did previously.  Now, two additional folders will exist, placed by the installer:  the classic `@` and `@home` folders.  `@` will supplant the previous (now renamed) `@` folder from the older version of Linux Mint; once verifying that no data from the old system is needed, the renamed subvolume can be deleted to free space.  Or kept for peace of mind—that's the beauty of subvolumes!  Because all user data exists in the renamed `@home` partition, the installer's `@home` partition can be deleted and replaced with the renamed `@home` partition (just rename it; the new system's `/etc/fstab` won't mind because the partition's UUID is the the same).  Double-check all work then unmount and reboot into a newly upgraded Linux Mint system.  

#### Results
After this process, login should work flawlessly; login with the credentials created by the installer and you should be have access to your previous `/home` folder.  If something has gone wrong, reboot to the live media and make sure that the renamed `@home` folder has been renamed and is in the proper location.

### Thanks
* [Adam Ryczkowski (Ask Ubuntu)](http://askubuntu.com/questions/457778/installing-14-04-server-to-exisiting-btrfs-subvolume) provided the original inspiration for the method described here and this is largely just an expanded writeup of his instructions.  He even tested this method on Linux Mint 16!
* [ArchWiki Btrfs guide](https://wiki.archlinux.org/index.php/Btrfs) is an excellent resource for making sure that whatever command you are about to run will do what you expect and nothing more.

