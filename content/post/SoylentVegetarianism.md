+++
date = "2015-03-28T11:14:36-06:00"
draft = false
title = "Soylent and Vegetarianism"
description = "How Soylent convinced me to try a vegetarian diet for 40 days."
categories = ["Food"]

+++

![1 Serving of Soylent](/SoylentVegetarianism/soylent.jpg)
## Lacto-ovo vegetarian
Five weeks ago, I decided to try something new. For Lent, I gave up all meat, including fish (why some people think fish is not meat, I don't know). Since then, my diet has been exclusively lacto-ovo vegetarian and will remain so until Easter Sunday. <!--more--> I had considered trying out a vegetarian diet before, and Lent provided an excellent opportunity: a reason to give up meat, a definite start date, and a definite end date. 

Somewhat unexpectedly, I do not miss the taste of meat. Getting away from it for several weeks has made me realize that I liked meat for its convenience rather than its taste. Previously, I could walk into [Rand](http://campusdining.vanderbilt.edu/location/rand-dining-center/ "Rand") (dining hall on Vanderbilt's campus) and order a grilled chicken breast; two minutes later, I would be eating a high-protein, low-fat, low-carbohydrate meal. So far, I have not found a replacement for this meal that is equally fast, healthy, and available.  

My new diet has gone surprisingly well, especially when I consider that I previous ate meat with almost every meal—bacon or sausage with breakfast, chicken, turkey, or ham with lunch, a chicken breast or occasional red meat with dinner. To make up for the loss of protein in my diet I have increased my serving size of protein shakes (from 24g/ day while eating meat to 48g/ day without meat). Counting macro-nutrients has never appealed to me, but from a quick assessment of how my body feels three weeks into my diet, the loss of meat has had no effect on my health. I exercise seven days a week and have no trouble recovering from workouts. 

### Why?
> "Nothing will benefit human health and increase the chances for survival of life on earth as much as the evolution to a vegetarian diet." —Albert Einstein

Meat production has a massive negative effect on the environment. Albert Einstein realized it, [Bill Gates](http://www.examiner.com/article/vegetarian-bill-gates-livestock-produces-51-of-world-s-greenhouse-gases "Bill Gates Vegetarian") realizes it, and I hope a solution can be found soon. Plant-based meat substitutes such as the ones Bill Gates has invested in show promise of satisfying the appeal of meat (taste and convenience) with far less impact than raising animals for human consumption. 

I did not give up meat because of any ethical reasons (much to PETA's chagrin, I'm sure). I was raised on meat from as soon as I was old enough to eat it (remember Vienna sausages?) and while I try to buy responsibly raised meat, I have no deeply seated aversion to the slaughter of animals for food. 

> Raising animals for human consumption accounts for approximately 40% of the total amount of agricultural output in industrialized countries. Grazing occupies 26% of the earth's ice-free terrestrial surface, and feed crop production uses about one third of all arable land. [1]

Rather, my greatest complaint about meat is [its wastefulness](https://en.wikipedia.org/wiki/Environmental_impact_of_meat_production). Feeding livestock requires diverting land to grazing that could otherwise be used for farming for human food. Beef represents the pinnacle of inefficiency, but all meat is wasteful when compared to raising plants for human consumption. Raising livestock diverts land and water that in many cases could produce far more produce per km^2 than meat. Coupled with the fact that livestock are notorious polluters through methane production, and meat clearly loses any sustainability argument. 

[1]: Wikipedia contributors; Steinfeld, H. et al. 2006. Livestock's Long Shadow: Environmental Issues and Options. Livestock, Environment and Development, FAO, Rome.

*Also, my sister bet I couldn't make it five days. Katherine, this one's for you.*

### Returning to an omnivore diet
I never had any intention of staying a vegetarian. High meat consumption remains a significant obstacle in reducing world hunger (due to use of land for grazing that could otherwise be farmed) and an increasingly worrying health risk in many countries. Despite the environmental costs, meat remains as popular as ever because People Like Meat. Meat may eventually go out of style when meat substitutes are well-regarded and ubiquitous but that day is far away. 

Until then, I hope meat will follow a similar path of gasoline. Like meat, gasoline is cheap (~$2.30/gal at time of writing), pervasive in both first and third-world countries, and horrible for the environment. In my family (and many others, I am sure) meat is treated similarly to gasoline: avoid it when possible and use sparingly when not. We have a saying which goes, "meat is a condiment, not an entree". 

## Inspiration
Now to address the picture of [Soylent](http://www.soylent.me/) at the top of this post. In October I joined the ranks of Famous Internet Person [@SwiftOnSecurity](https://twitter.com/SwiftOnSecurity/status/581963827371114496) and [Ars Technica's Lee Hutchinson](http://arstechnica.com/gadgets/2013/08/nothing-but-the-soylent-were-trying-1-full-week-of-the-meal-substitute/) with a one week supply of Soylent v1.1. Soylent is a meal replacement powder designed to be mixed up once a day and then consumed at meal times and for snacks throughout the day. It is nutritionally complete—carbohydrates, protein, fat, fiber, vitamins—so in theory one could live off it. I have not, although I came close to doing so during finals week at Vanderbilt.

The newest version (v1.4) does not require mixing with oil; my version (v1.1) requires a small vial of oil to be mixed with each day's serving. As a refresher, v1.1 was a modest reformulation of Soylent with less sweetener and "digestive enzymes" to reduce gastric unrest. It must have worked because I have no problems digesting Soylent. 

I describe Soylent as "nutrition, not food" to people who ask. While a Soylent shake does have a taste to it, it is a vaguely sweet and chalky taste with a possible hint of oats. No flavor in Soylent is overpowering so other flavors (chocolate syrup) can easily be mixed in. The main appeal of Soylent is its convenience and nutrition. I can make a day's worth of Soylent in a few minutes and then be freed from worrying about finding healthy food choices for the rest of the day. 

After such a positive experience with Soylent (vegetarian except for the fish oil that is added to raise fat content) I decided to try a full vegetarian diet. Five weeks later, I'm healthy, meat-free (for seven more days), and remain confident that reduced meat consumption will be an important part of sustainable agriculture in the near future. 