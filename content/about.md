+++
date = "2014-01-01T20:41:11-06:00"
draft = false
title = "About Me & Contact Info"
menu = "main"

+++
Joseph Stahl is a computer science and pre-med student at Vanderbilt University.  In his spare time, he can be found marching with the Vanderbilt Spirit of Gold in the fall, trying to keep up with F1 racing in the spring, repeatedly breaking and fixing this website, and reading a book a few pages at a time.  He spends the school year in Nashville, TN but still calls Birmingham, AL his home.  

This website is meant primarily as a way to get my name out and distribute my résumé, but it also will serve as a platform for me to write about my experiences both on campus and away from Vanderbilt, with a particular focus on topics related to medicine and computers.  Perhaps a few posts about current events, Nashville events, and whatever's on my mind.

--------
## Other Links
[Bitbucket](https://bitbucket.org/josephst18)  
[Github](https://github.com/josephst) 


### Contact Me
**Joseph Stahl** 	
2301 Vanderbilt Pl.  
PMB 355703  
Nashville, TN 37235-5703  

[josephstahl.com](josephstahl.com)	
[joseph@josephstahl.com](mailto:joseph@josephstahl.com)  

[GPG Key](/media/gpgKey/josephPubKey.asc)