# License

----------

![License](https://i.creativecommons.org/l/by/4.0/88x31.png "Creative Commons License")
This work is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/)